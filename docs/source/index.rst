.. Django demo project documentation master file, created by
   sphinx-quickstart on Wed Apr 17 00:41:37 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Django demo project's documentation!
===============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   code.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
